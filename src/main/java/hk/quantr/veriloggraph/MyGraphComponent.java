package hk.quantr.veriloggraph;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import hk.quantr.mxgraph.swing.mxGraphComponent;
import hk.quantr.mxgraph.swing.view.mxInteractiveCanvas;
import hk.quantr.mxgraph.view.mxGraph;

public class MyGraphComponent extends mxGraphComponent {

	public MyGraphComponent(mxGraph graph) {
		super(graph);
	}

	@Override
	protected void paintGrid(Graphics g) {
		double scale = this.getGraph().getView().getScale();
		super.paintGrid(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(Color.gray);
		int x;
		int y = 0;
		int height = Math.max((int) this.getScaledPreferredSizeForGraph().getHeight(), this.getHeight());
		int width = Math.max((int) this.getScaledPreferredSizeForGraph().getWidth(), this.getWidth());
		while (y < height) {
			x = 0;
			while (x < width) {
				g2.fillRect(x, y, 1, 1);
				x += (scale * 20) / 2;
			}
			y += (scale * 20) / 2;
		}
	}

	@Override
	public mxInteractiveCanvas createCanvas() {
		return new PeterSwingCanvas(this);
	}

}

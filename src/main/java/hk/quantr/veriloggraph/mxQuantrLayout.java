package hk.quantr.veriloggraph;

import hk.quantr.mxgraph.layout.mxGraphLayout;
import hk.quantr.mxgraph.model.mxIGraphModel;
import hk.quantr.mxgraph.view.mxGraph;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class mxQuantrLayout extends mxGraphLayout {

	protected mxGraph graph;

	public mxQuantrLayout(mxGraph graph) {
		super(graph);
		this.graph = graph;
	}

	@Override
	public void execute(Object parent) {
		mxIGraphModel model = graph.getModel();
		int childCount = model.getChildCount(parent);

		for (int i = 0; i < childCount; i++) {
			Object cell = model.getChildAt(parent, i);
			if (!isVertexIgnored(cell)) {
				if (isVertexMovable(cell)) {
					setVertexLocation(cell, 100, i * 200);
				}
			}
		}
	}

//	@Override
//	public void moveCell(Object cell, double x, double y) {
//		throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
//	}
}

package hk.quantr.veriloggraph;

import hk.quantr.mxgraph.model.mxCell;
import java.awt.Color;

import hk.quantr.mxgraph.swing.mxGraphComponent;
import hk.quantr.mxgraph.swing.view.mxInteractiveCanvas;
import hk.quantr.mxgraph.view.mxCellState;
import hk.quantr.verilogcompiler.structure.Port;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Objects;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class PeterSwingCanvas extends mxInteractiveCanvas {

//	CellRendererPane rendererPane = new CellRendererPane();
//	ModulePanel panel = new ModulePanel();
	mxGraphComponent graphComponent;

//	Color borderColor = new Color(255, 0, 0);
//	Color backgroundcolor = new Color(230, 230, 230);
	Color highlightBackground = Color.red;
	ArrayList<mxCell> selectedCells = new ArrayList<>();

	public PeterSwingCanvas(mxGraphComponent graphComponent) {
		this.graphComponent = graphComponent;

//		jLabel.setBorder(BorderFactory.createLineBorder(borderColor));
//		jLabel.setHorizontalAlignment(JLabel.CENTER);
//		jLabel.setBackground(backgroundcolor);
//		jLabel.setOpaque(true);
//		jLabel.setFont(new Font("arial", Font.PLAIN, 10));
	}

	public void drawVertex(mxCellState state, String label) {
//		ModuleLib.log("drawVertex " + label + ", " + state.getWidth() + ", " + state.getHeight());
//		panel.setOpaque(true);
//		panel.nameLabel.setText("FUCK" + label);
//		panel.nameLabel.setForeground(Color.black);
//
//		rendererPane.paintComponent(g, panel, graphComponent, (int) (state.getX() + translate.getX()), (int) (state.getY() + translate.getY()), (int) state.getWidth(), (int) state.getHeight());

		JLabel jLabel = new JLabel(label);
		jLabel.setOpaque(true);
		jLabel.setHorizontalAlignment(SwingConstants.CENTER);

		mxCell cell = (mxCell) state.getCell();
		if (cell.getValue() instanceof Port) {
			Port port = (Port) cell.getValue();
			jLabel.setText(port.name + Objects.toString(port.range, ""));
			if (selectedCells.contains(cell)) {
				jLabel.setBackground(highlightBackground);
//				jLabel.setBorder(new LineBorder(Color.decode("#cdb4db"), 4));
			} else {
				if (port.direction.equals("input")) {
					jLabel.setBackground(Color.decode("#d8eeff"));
				} else {
					jLabel.setBackground(Color.decode("#ffeed8"));
				}
			}
			double scale = state.getView().getScale();
			jLabel.setFont(new Font("arial", Font.PLAIN, (int) (12 * scale)));
		} else if (cell.getValue() instanceof hk.quantr.verilogcompiler.structure.Module) {
			hk.quantr.verilogcompiler.structure.Module module = (hk.quantr.verilogcompiler.structure.Module) cell.getValue();
			jLabel.setText(module.name);
			double scale = state.getView().getScale();
			jLabel.setFont(new Font("arial", Font.BOLD, (int) (16 * scale)));
			jLabel.setBackground(Color.decode("#b8deff"));
			jLabel.setBorder(new EmptyBorder(10, 0, 0, 0));
			jLabel.setVerticalAlignment(SwingConstants.TOP);
		} else {
			jLabel.setText(label);
		}
		rendererPane.paintComponent(g, jLabel, graphComponent, (int) (state.getX() + translate.getX()), (int) (state.getY() + translate.getY()), (int) state.getWidth(), (int) state.getHeight(), true);
	}

}

# Introduction
		
This java program is to visualize the verilog code

## Run it

java -jar your_verilog.v

## Embed it as a JComponent into your java swing app

```
QuantrVerilogGraphPanel quantrVerilogGraphPanel=new QuantrVerilogGraphPanel();
quantrVerilogGraphPanel.loadAllVerilog(new File("your_verilog.v"));
getContentPane().add(quantrVerilogGraphPanel, java.awt.BorderLayout.CENTER);  // Just add it on JFrame/JPanel or whatever JComponent
```

# Author

Peter, System Architect <peter@quantr.hk>

# Screenshot

![](https://gitlab.com/quantr/toolchain/verilog-graph/-/raw/master/screenshot/20220418.png)

